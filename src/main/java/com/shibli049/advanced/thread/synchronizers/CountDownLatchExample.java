package com.shibli049.advanced.thread.synchronizers;

import java.util.concurrent.CountDownLatch;

/**
 * Created by github.com/shibli049 on 2/10/18.
 */
public class CountDownLatchExample {

  public static void main(String[] args) {
    CountDownLatch start = new CountDownLatch(1);
    CountDownLatch end = new CountDownLatch(4);

    for (int i = 0; i < 5; i++) {
      new Thread(new Worker(start, end)).start();
    }

    try {
      System.out.println("main thread is doing something.");
      start.countDown();
      Thread.sleep(1000);
      System.out.println("main thread is doing something else.");
      end.await();
      System.out.println("main thread ended.");
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

  private static class Worker extends Thread {

    private final CountDownLatch start;
    private final CountDownLatch end;

    public Worker(CountDownLatch start,
        CountDownLatch end) {
      this.start = start;
      this.end = end;
    }


    @Override
    public void run() {
      try {
        System.out.println("Worker: Thread entered run");
        start.await();
        System.out.println("Worker: doing work");
        Thread.sleep(3000);
        end.countDown();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
