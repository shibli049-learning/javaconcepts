package com.shibli049.advanced.thread.join;

/**
 * Created by github.com/shibli049 on 2/10/18.
 */
public class ThreadJoinMain {

  public static void main(String[] args) {
//    System.out.println("test1");
//    test1();


    System.out.println("test2");
    test2();
  }

  private static void test1() {
    TestJoinClass t1 = new TestJoinClass("t1");
    TestJoinClass t2 = new TestJoinClass("t2");
    TestJoinClass t3 = new TestJoinClass("t3");
    t1.start();

    try {
      t1.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    t2.start();
    t3.start();
  }

  private static void test2() {
    TestJoinClass t1 = new TestJoinClass("t1");
    TestJoinClass t2 = new TestJoinClass("t2");
    TestJoinClass t3 = new TestJoinClass("t3");
    t1.start();
    t2.start();

    try {
      t2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    t3.start();
  }
}
