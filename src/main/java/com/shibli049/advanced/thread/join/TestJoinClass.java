package com.shibli049.advanced.thread.join;

/**
 * Created by github.com/shibli049 on 2/10/18.
 */
public class TestJoinClass extends Thread {

  public TestJoinClass(String name){
    super(name);
  }

  @Override
  public void run() {
    for (int i = 0; i < 5; i++) {
      try{
        Thread.sleep(500);
      }catch (Exception ex){
        System.out.println(ex);
      }

      System.out.println(Thread.currentThread().getName() + " i = " + i);
    }
  }


}
