package com.shibli049.advanced.thread.join;



/**
 * Created by github.com/shibli049 on 2/10/18.
 */
public class ThreadSynchronization {

  public static void main(String[] args) throws InterruptedException {
    threadTest();

  }

  private static void threadTest() {
    Runnable r = () -> {
      System.out.println("tname: " + Thread.currentThread().getName() + ", ID: " + getID());
    };

    Thread t1 = new Thread(r, "t1");
    t1.start();
    Thread t2 = new Thread(r, "t2");
    t2.start();
  }

  static int counter = 1;

  public static int getID(){
    return counter++;
  }
}
