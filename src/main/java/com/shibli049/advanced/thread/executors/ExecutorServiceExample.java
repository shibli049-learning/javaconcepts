package com.shibli049.advanced.thread.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by github.com/shibli049 on 2/10/18.
 */
public class ExecutorServiceExample {

  public static void main(String[] args) {
    ExecutorService executorService = Executors.newFixedThreadPool(5);
    for (int i = 0; i < 10; i++) {
      Runnable worker = new WorkerThread("Thread" + i);
      executorService.execute(worker);
    }

    executorService.shutdown();
    while (!executorService.isTerminated()){
      //waiting
    }

    System.out.println("all task finished.");

  }

  private static class WorkerThread implements Runnable {

    private final String name;

    public WorkerThread(String name) {
      this.name = name;
    }


    @Override
    public void run() {
      System.out.println(Thread.currentThread().getName() + " (start) " + this.name);
      workToBeDone();
      System.out.println(Thread.currentThread().getName() + " (end) ");

    }

    private void workToBeDone() {
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
