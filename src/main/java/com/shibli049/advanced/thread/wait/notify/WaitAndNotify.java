package com.shibli049.advanced.thread.wait.notify;

/**
 * Created by github.com/shibli049 on 2/10/18.
 */
public class WaitAndNotify {

  public static void main(String[] args) {
    ThreadB b = new ThreadB();
    b.start();
    synchronized (b){
      try {
        System.out.println("waiting for the other thread to complete....");
        b.wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      System.out.println("total is: " + b.total);
    }

  }



}

class ThreadB extends Thread{
  int total;

  @Override
  public void run() {
    synchronized (this){
      for (int i = 1; i <= 10; i++) {
        total += i;
      }

      notify();
    }
  }
}
