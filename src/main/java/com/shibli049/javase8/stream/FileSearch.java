package com.shibli049.javase8.stream;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by github.com/shibli049 on 2/12/18.
 */
public class FileSearch {
    public static void main(String[] args) {
        readFile();
    }

    private static void readFile() {
        Path path = FileSystems.getDefault().getPath("files", "Iliad.txt");
        final String searchTerm = "achilles";

        try (Stream<String> lines = Files.lines(path).parallel()){
            Optional<String> line = lines
                    .filter(l -> l.toLowerCase().contains(searchTerm))
                    .findAny();

            if(line.isPresent()) {
                line.ifPresent(l -> System.out.println("Found: " + l));
            }else{
                String s = line.orElse("not found.");
                System.out.println(s);
                Object obj = new Object();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
