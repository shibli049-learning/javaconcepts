package com.shibli049.javase8.stream;

import com.shibli049.javase8.filter.with.predicate.PredicateFilterExample;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

/**
 * Created by github.com/shibli049 on 2/11/18.
 */
public class StreamExample {

  public static void main(String[] args) {
    test2();
  }

  private static void test2() {
    long sum = PredicateFilterExample.getPersonList()
        .stream().mapToInt(p -> p.getAge())
        .sum();
    System.out.println("age sum: " + sum);

    OptionalDouble dbl = PredicateFilterExample.getPersonList()
        .stream().mapToInt(p -> p.getAge())
        .average();

    if (dbl.isPresent()) {
      System.out.println("age avg: " + dbl.getAsDouble());
    }else {
      System.out.println("age avg(n/a): " + 0);
    }
  }

  private static void test1() {
    List<String> strings = getStrings();
    System.out.println("stream");
    long startTime = System.currentTimeMillis();
    System.out.println(strings.stream().count());
    long streamTime = System.currentTimeMillis()-startTime;
    System.out.println(streamTime);

    System.out.println("parallel stream");
    startTime = System.currentTimeMillis();
    System.out.println(strings.stream().parallel().count());;
    long pstreamTime = System.currentTimeMillis()-startTime;
    System.out.println(pstreamTime);
    System.out.println(streamTime/(float)pstreamTime);

  }

  private static List<String> getStrings() {
    List<String> strings = new ArrayList<>();
    for (int i = 0; i < 100_000; i++) {
      strings.add("s"+i);
    }

    return strings;
  }
}
