package com.shibli049.javase8.interfaces;

/**
 * Created by github.com/shibli049 on 2/10/18.
 */

@FunctionalInterface
public interface SimpleInterface {
  public void doSomething();
  public default void doSomethingElse(){
    System.out.println("doSomethingElse");
  }
}
