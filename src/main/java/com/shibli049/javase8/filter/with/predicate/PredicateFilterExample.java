package com.shibli049.javase8.filter.with.predicate;

import com.shibli049.javase8.filter.with.predicate.domain.Person;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by github.com/shibli049 on 2/10/18.
 */
public class PredicateFilterExample {

  public static void main(String[] args) {
    test1();
  }

  private static void test1() {
    List<Person> personList = getPersonList();
    Predicate<Person> younger = person -> person.getAge() < 18;
    Predicate<Person> older = person -> person.getAge() > 65;
    Predicate<Person> nameis = person -> person.getName().equalsIgnoreCase("jen");

    personList.stream()
        .filter(nameis.and(younger.or(older)).negate())
        .forEach(System.out::println);

  }

  public static List<Person> getPersonList() {
    return Arrays.asList(
        new Person("Jen", 15),
        new Person("Mon", 18),
        new Person("Monk", 9),
        new Person("Jen", 27),
        new Person("Kom", 70),
        new Person("Jen", 66)
    );
  }
}
