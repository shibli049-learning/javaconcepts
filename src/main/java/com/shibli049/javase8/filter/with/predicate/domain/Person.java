package com.shibli049.javase8.filter.with.predicate.domain;

/**
 * Created by github.com/shibli049 on 2/10/18.
 */
public class Person {

  private String name;
  private int age;

  public Person() {
  }

  public Person(String name, int age) {
    this.name = name;
    this.age = age;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  @Override
  public String toString() {
    return "Person{" +
        "name:'" + name + '\'' +
        ", age:" + age +
        '}';
  }
}

