package com.shibli049.javase8.join.string;

import java.util.StringJoiner;
import java.util.stream.Stream;

/**
 * Created by github.com/shibli049 on 2/11/18.
 */
public class JoinerMethod {

    public static void main(String[] args) {
        stringjoin();

         stringJoiner();
    }

    private static void stringJoiner() {
        String[] names = {"Shibli", "Rizwan", "Sun"};
        StringJoiner sj = new StringJoiner("," , "{", "}");
        sj.setEmptyValue("{}");
        System.out.println(sj);

        sj.add("Alauddin").add("Tamim");

        Stream.of(names)
                .forEachOrdered(sj::add);
        System.out.println(sj);

        StringJoiner sj1 = new StringJoiner(",", "(", ")");
        sj1.add("Faisal")
        .add("Zillur");

        sj.merge(sj1);
        System.out.println(sj);


    }

    private static void stringjoin() {
        String joined = String.join(" and ", "Shibli", "Rizwan", "Sun");
        System.out.println(joined);
    }
}
