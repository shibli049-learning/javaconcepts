package com.shibli049.javase8;

import com.shibli049.javase8.interfaces.ArgsInterface;
import com.shibli049.javase8.interfaces.SimpleInterface;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by github.com/shibli049 on 2/10/18.
 */
public class LambdaImpl {

  public static void main(String[] args) {
    SimpleInterface simpleInterfaceImpl = ()-> System.out.println("hello from lambda.");
    simpleInterfaceImpl.doSomething();

    ArgsInterface argsInterface = (x, y) -> {
      int result = x - y;
      System.out.println(result);
    };

    argsInterface.doSomething(1, 5);

    useComparator();

  }

  private static void useComparator() {
    List<String> list = Arrays.asList("AAA",
        "BBB",
        "ccc",
        "dDD",
        "Ddd",
        "EEE");

    Collections.sort(list);
    list.forEach(System.out::println);
    System.out.println("case ignored::");
    Collections.sort(list, (a, b) -> b.compareToIgnoreCase(a));
    list.forEach(System.out::println);

    System.out.println("parallel-stream");
    list.stream().parallel().forEach(System.out::println);

  }
}
