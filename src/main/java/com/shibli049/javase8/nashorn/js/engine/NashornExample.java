package com.shibli049.javase8.nashorn.js.engine;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.*;

/**
 * Created by github.com/shibli049 on 2/11/18.
 */
public class NashornExample {

    public static void main(String[] args) {
        executeJsFile();
//        testJsEngine();
    }

    private static void executeJsFile() {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

        File file = new File("scripts/readurl.js");
        try (Reader reader = new FileReader(file)) {
            Object result = engine.eval(reader);
            System.out.println("errere");
            System.out.println(result);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }

    private static void testJsEngine() {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

        String script = "var welcome = 'Hello';" +
                "welcome += ', World!!';" +
                "welcome;";
        try {
            Object result = engine.eval(script);
            System.out.println(result);
        } catch (ScriptException e) {
            e.printStackTrace();
        }

    }
}
