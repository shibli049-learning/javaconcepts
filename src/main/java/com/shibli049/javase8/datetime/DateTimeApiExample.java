package com.shibli049.javase8.datetime;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoField;
import java.util.Locale;

/**
 * Created by github.com/shibli049 on 2/11/18.
 */
public class DateTimeApiExample {
    public static void main(String[] args) throws InterruptedException {

        timeZones();
        System.out.println();

//        formatDateTime();
//        System.out.println();

//        testDateAndTime();
//        System.out.println();

//        testInstantAndDuration();
//        System.out.println();

    }

    private static void timeZones() {
        System.out.println("timeZones");
        DateTimeFormatter dtf = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println(dtf.format(dateTime));

        ZonedDateTime gmt = ZonedDateTime.now(ZoneId.of("GMT+0"));
        System.out.println(dtf.format(gmt));

        ZonedDateTime bdt = ZonedDateTime.now(ZoneId.of("GMT+06"));
        System.out.println(dtf.format(bdt));

        ZonedDateTime unknown = ZonedDateTime.now(ZoneId.of("GMT+05:55"));
        System.out.println(dtf.format(unknown));


        ZonedDateTime dhk = ZonedDateTime.now(ZoneId.of("GMT+05:55"));
        System.out.println(dtf.format(unknown));

        System.out.println("zone ids:");
        ZoneId.getAvailableZoneIds()
                .stream()
                .filter(zoneID -> zoneID.contains("Dhaka"))
                .forEach(System.out::println)
        ;

    }

    private static void formatDateTime() {
        System.out.println("formatDateTime");

        LocalDate date = LocalDate.of(2018, 2, 25);
        DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE;
        System.out.println(dtf.format(date));

        LocalTime time = LocalTime.now();
        dtf = DateTimeFormatter.ISO_TIME;
        System.out.println(dtf.format(time));

        LocalDateTime dateTime = LocalDateTime.of(date, time);
        dtf = DateTimeFormatter.ISO_DATE;
        System.out.println(dtf.format(dateTime));

        dtf = DateTimeFormatter.ISO_TIME;
        System.out.println(dtf.format(dateTime));

        dtf = DateTimeFormatter.ISO_DATE_TIME;
        System.out.println(dtf.format(dateTime));


        System.out.println("other");

        System.out.println("full");
        DateTimeFormatter full_format = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
        System.out.println(full_format.format(dateTime));

        System.out.println("long");
        DateTimeFormatter long_format = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
        System.out.println(long_format.format(dateTime));

        System.out.println("short");
        DateTimeFormatter short_format = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
        System.out.println(short_format.format(dateTime));

        String fr_short = short_format.withLocale(Locale.FRENCH).format(dateTime);
        String fr_long = long_format.withLocale(Locale.FRENCH).format(dateTime);
        System.out.println(fr_short);
        System.out.println(fr_long);

        System.out.println("custom");

        DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder()
                .appendValue(ChronoField.YEAR)
                .appendLiteral("@")
                .appendValue(ChronoField.MONTH_OF_YEAR)
                .appendLiteral("@")
                .appendValue(ChronoField.DAY_OF_MONTH)
                ;

        System.out.println(builder.toFormatter().format(dateTime));

    }

    private static void testDateAndTime() {
        System.out.println("DateAndTime");

        System.out.println("date");
        LocalDate notDate = LocalDate.now();
        System.out.println(notDate);

        LocalDate anotherDate = LocalDate.of(2014, 10, 31);
        System.out.println(anotherDate);

        anotherDate = LocalDate.of(1969, 5, 31);
        System.out.println(anotherDate);

        System.out.println("time:");
        LocalTime nowTime = LocalTime.now();
        System.out.println(nowTime);

        LocalTime anotherTime = LocalTime.of(13, 10, 15, 999_999_999);
        System.out.println(anotherTime);

        System.out.println("date time:");
        LocalDateTime nowDateTime = LocalDateTime.now();
        System.out.println(nowDateTime);

        LocalDateTime anotherDateTime = LocalDateTime.of(anotherDate, anotherTime);
        System.out.println(anotherDateTime);


    }

    private static void testInstantAndDuration() throws InterruptedException {
        System.out.println("InstantAndDuration");
        Instant start = Instant.now();
        System.out.println(start);

        Thread.sleep(100);

        Instant end = Instant.now();
        System.out.println(end);

        Duration elapsed = Duration.between(start, end);

        System.out.println(elapsed.toMillis());
    }
}
